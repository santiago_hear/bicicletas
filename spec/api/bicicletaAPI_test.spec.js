///////////////// TEST MONGOOSE API ////////////////////////////////
var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

var url_bicis = "http://localhost:3000/api/bicicletas";

describe('bicicleta API', () => {

    afterEach( (done) => {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            done();
        });
    });

    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            request.get(url_bicis, (error, response, body) => {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var Bici = '{ "idBici": 10, "color": "verde", "modelo" : "urbana", "lat" : 5.067455, "long" : -75.524063 }';

            request.post({
                headers: headers,
                url: url_bicis + '/create',
                body: Bici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                var Rbici = JSON.parse(body).bicicleta;
                expect(Rbici.color).toBe('verde');
                expect(Rbici.ubicacion[0]).toBe(5.067455);
                expect(Rbici.ubicacion[1]).toBe(-75.524063);
                done();
            });
        });
    });

    describe('DELETE BICICLETAS /delete', () => {
        it('Status 204', (done) => {
            var bici = Bicicleta.createInstance(1, 'negro', 'urbana', [5.067455, -75.524063])

            Bicicleta.add(bici, (error1, newBici) => {
                Bicicleta.allBicis( (error2, bicis) => {
                    cantBicis = bicis.length;

                    var headers = {'content-type' : 'application/json'};
                    var bicidel = '{ "idBici": 1}';

                    request.delete({
                        headers: headers,
                        url: url_bicis + '/delete',
                        body: bicidel
                    }, function(error3, response, body) {
                        Bicicleta.allBicis( (error4, allbicis) => {
                            expect(response.statusCode).toBe(204);
                            expect(allbicis.length).toBe(cantBicis - 1);
                        });
                        done();
                    });
                });
                
            });
        });
    });
});



////////////////// TEST INICIAL API ///////////////////////////////

// var Bicicleta = require('../../models/bicicleta');
// var request = require('request');
// var server = require('../../bin/www');

// describe('bicicleta API', () => {
//     describe('GET BICICLETAS /', () => {
//         it('Status 200', () => {
//             expect(Bicicleta.allBicis.length).toBe(0);

//             var Bici =  new Bicicleta(1, 'rojo', 'urbana', [5.067455, -75.524063]);
//             Bicicleta.add(Bici);

//             request.get('http://localhost:3000/api/bicicletas', function(error, response, body) {
//                 expect(response.statusCode).toBe(200);
//             });
//         });
//     });
//     describe('POST BICICLETAS /create', () => {
//         it('status 200', (done) => {
//             var headers = {'content-type' : 'application/json'};
//             var Bici = '{ "id": 10, "color": "verde", "modelo" : "urbana", "lat" : 5.067455, "long" : -75.524063 }';

//             request.post({
//                 headers: headers,
//                 url: 'http://localhost:3000/api/bicicletas/create',
//                 body: Bici
//             }, function(error, response, body) {
//                 expect(response.statusCode).toBe(200);
//                 // expect(Bicicleta.findById(10).color).toBe('verde');
//                 done();
//             });
//         });
//     });
//     describe('POST BICICLETAS /update', () => {
//         it('status 200', (done) => {
//             var headers = {'content-type' : 'application/json'};
//             var Bici = '{ "id": 10, "color": "azul", "modelo" : "urbana", "lat" : 5.067455, "long" : -75.524063 }';

//             request.post({
//                 headers: headers,
//                 url: 'http://localhost:3000/api/bicicletas/update',
//                 body: Bici
//             }, function(error, response, body) {
//                 expect(response.statusCode).toBe(200);
//                 // expect(Bicicleta.findById(10).color).toBe('azul');
//                 done();
//             });
//         });
//     });
//     describe('DELETE BICICLETAS /delete', () => {
//         it('status 200', (done) => {
//             var headers = {'content-type' : 'application/json'};
//             var IdBici = '{ "id": 10 }';
//             var cantBicis = Bicicleta.allBicis.length;

//             request.delete({
//                 headers: headers,
//                 url: 'http://localhost:3000/api/bicicletas/delete',
//                 body: IdBici
//             }, function(error, response, body) {
//                 expect(response.statusCode).toBe(200);
//                 expect(Bicicleta.allBicis.length).toBe(cantBicis -1);
//                 done();
//             });
//         });
//     });
// });