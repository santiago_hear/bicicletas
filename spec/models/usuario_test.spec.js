var mongoose = require('mongoose');
var Usuario = require('../../models/usuario');
var Bicicleta = require('../../models/bicicleta')
var Reserva = require('../../models/reserva');

describe('resting Usuarios', () => {
    beforeEach((done) => {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser : true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database');
        });
        done();
    });

    afterEach((done) => {
        Reserva.deleteMany({}, (err, success) => {
            if (err) console.log(err);
            Usuario.deleteMany({} , (err, success) => {
                if (err) console.log(err);
                Bicicleta.deleteMany({}, (err, success) => {
                    if (err) console.log(err);
                    done();
                });
            });
        });
    });

    describe('Reserva de Bici', () => {
        it('debe exisitr una reserva', (done) => {
            const user = new Usuario({nombre: 'Santiago'});
            user.save();
            const bici = Bicicleta.createInstance(1,"verde", "mountain", [5.067455, -75.524063]);
            bici.save();

            var hoy = new Date();
            var mañana = new Date();
            mañana.setDate(hoy.getDate() + 1);

            user.reservar(user._id ,bici._id, hoy, mañana, (err, reserva) => {
                Reserva.find({}).populate('bicicleta').populate('usuario').exec((err, reservas) => {
                    console.log(reservas[0]);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.idBici).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(user.nombre);
                    done();
                });
            });
        });
    });
});