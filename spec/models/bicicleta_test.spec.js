var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

///////////////// TEST MONGOOSE /////////////////////////

describe('Testing Bicicletas', function() {
    beforeEach((done) => {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser : true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database');
        });
        done();
    });

    afterEach(function (done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            done();
        });
    });

    describe('createInstance', () => {
        it('crea una instancia de una Bici', () => {
            var bici = Bicicleta.createInstance(1,"verde", "mountain", [5.067455, -75.524063]);

            expect(bici.idBici).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("mountain");
            expect(bici.ubicacion[0]).toEqual(5.067455);
            expect(bici.ubicacion[1]).toEqual(-75.524063);
        })
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis( (err, bicis) => {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('Agragamos una bicicleta', (done) => {
            var Bici =  new Bicicleta({idBici: 1, color: 'verde', modelo: 'urbana'});
            Bicicleta.add(Bici, (err,  newBici) => {
                if (err) console.log(err);
                Bicicleta.allBicis((err,  bicis) => {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].idBici).toEqual(Bici.idBici);
                    done();
                });
            });
        });
    });

    describe('Bicicleta.findById', () => {
        it('Buscar bicicleta por Id', (done) => {
            Bicicleta.allBicis((err, bicis) => {
                expect(bicis.length).toBe(0);

                var Bici1 =  new Bicicleta({idBici: 1, color: 'verde', modelo: 'urbana'});

                Bicicleta.add(Bici1, (err, newBici) => {
                    if (err) console.log(err);

                    var Bici2 = new Bicicleta({idBici : 2, color: "roja", modelo: "urbana"});
                    Bicicleta.add(Bici2, (error, newBici) => {
                        if (err) console.log(err);
                        Bicicleta.findById(1, (error, targetBici) => {
                            expect(targetBici.idBici).toBe(Bici1.idBici);
                            expect(targetBici.color).toBe(Bici1.color);
                            expect(targetBici.modelo).toBe(Bici1.modelo);
                            done();
                        });
                    });
                });
            });
        });
    });
    describe('Bicicleta.removeById', () => {
        it('Borrar bicicleta por Id', (done) => {
            Bicicleta.allBicis((err, bicis) => {
                expect(bicis.length).toBe(0);

                var Bici1 =  new Bicicleta({idBici: 1, color: 'verde', modelo: 'urbana'});

                Bicicleta.add(Bici1, (err, newBici) => {
                    if (err) console.log(err);

                    var Bici2 = new Bicicleta({idBici : 2, color: "roja", modelo: "urbana"});
                    Bicicleta.add(Bici2, (error, newBici) => {
                        if (err) console.log(err);
                        Bicicleta.allBicis((err, allbicis) => {
                            Bicicleta.removeById(1, (error, deleteBici) => {
                                Bicicleta.allBicis((err, allbicisD) => {
                                    expect(allbicisD.length).toBe(allbicis.length - 1);
                                    done();
                                });
                            });
                        });
                    });
                });
            });
        });
    });
});



///////////////// TEST MODELO INCIAL //////////////////////

// beforeEach(() => {
//     Bicicleta.allBicis = []
// });

// describe('Bicicleta.allBicis', () => {
//     it('comienza vacia', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//     });
// });

// describe('Bicicleta.add', () => {
//     it('Agragamos bicicleta', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);

//         var Bici =  new Bicicleta(1, 'rojo', 'urbana', [5.067455, -75.524063]);
//         Bicicleta.add(Bici);

//         expect(Bicicleta.allBicis.length).toBe(1);
//         expect(Bicicleta.allBicis[0]).toBe(Bici);
//     });
// });

// describe('Bicicleta.findbyid', () => {
//     it('buscar bici', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);

//         var Bici =  new Bicicleta(1, 'rojo', 'urbana', [5.067455, -75.524063]);
//         var Bici2 = new Bicicleta(2, 'blanco', 'urbana', [5.068043, -75.511789]);
//         Bicicleta.add(Bici);
//         Bicicleta.add(Bici2);

//         var targetBici = Bicicleta.findById(1);

//         expect(targetBici.id).toBe(1);
//         expect(targetBici.color).toBe(Bici.color);
//         expect(targetBici.modelo).toBe(Bici.modelo);
//     });
// });

// describe('Bicicleta.deletebyid', () => {
//     it('eliminar bici', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);

//         var Bici =  new Bicicleta(1, 'rojo', 'urbana', [5.067455, -75.524063]);
//         var Bici2 = new Bicicleta(2, 'blanco', 'urbana', [5.068043, -75.511789]);
//         Bicicleta.add(Bici);
//         Bicicleta.add(Bici2);

//         var longArr = Bicicleta.allBicis.length;

//         Bicicleta.removeById(1);

//         expect(Bicicleta.allBicis.length).toBe(longArr - 1);
//     });
// });