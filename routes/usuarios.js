var express = require('express');
var router = express.Router();
var usuarioController = require('../controllers/usuarios');

router.get('/', usuarioController.usuarios_list);

router.get('/create', usuarioController.usuarios_create_get);
router.post('/create', usuarioController.usuarios_create_post);

router.get('/:id/update', usuarioController.usuarios_update_get);
router.post('/:id/update', usuarioController.usuarios_update_post);

router.post('/:id/delete', usuarioController.usuarios_delete);

module.exports = router;