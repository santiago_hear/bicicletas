var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function(req, res){
    Bicicleta.allBicis( (err, bicis) => {
        if(err) console.log(err);
        res.render('bicicletas/index', {bicis: bicis});
    });
}

exports.bicicleta_create_get = function(req, res){
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function(req, res){
    
    var bici = new Bicicleta({idBici: req.body.id, color: req.body.color, modelo: req.body.modelo});
    bici.ubicacion = [req.body.lat, req.body.long];
    Bicicleta.add(bici, (err) => {
        if(err) console.log(err);
        res.redirect('/bicicletas');
    });
}

exports.bicicleta_delete_post = function(req, res) {
    Bicicleta.removeById(req.params.id, (err) => {
        if(err) console.log(err);
        res.redirect('/bicicletas');
    });
}

exports.bicicleta_update_get = function(req, res){
    console.log(req.params.id);
    Bicicleta.findById(req.params.id, (err, bici) => {
        if(err) console.log(err);
        res.render('bicicletas/update',{bici: bici});
    });
}

exports.bicicleta_update_post = function(req, res){

    Bicicleta.findByIdAndUpdate(req.body.id, {idBici: req.body.idBici, color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.long]}, (err) => {
        if(err) console.log(err);
        res.redirect('/bicicletas');
    });
}