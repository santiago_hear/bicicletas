var Usuario = require('../../models/usuario');

exports.usuarios_list = function(req, res){
    console.log('obteniendo usuarios');
    Usuario.find({}, (err, users) => {
        res.status(200).json({
            usuarios : users
        });
    });
};

exports.usuarios_create = function(req, res){
    console.log('creando user');
    var user = new Usuario({nombre: req.body.nombre});

    user.save((err) => {
        res.status(200).json({user});
    });
};

exports.usuario_reservar = (req, res) => {
    Usuario.findById(req.body.id , (err, user) => {
        console.log(user);
        user.reservar(req.body.idBici, req.body.fecha_ini, req.body.fecha_fin, (err) => {
            console.log('reservando');
            res.status(200).send();
        });
    });
};