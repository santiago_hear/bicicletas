var Usuario = require('../models/usuario');

module.exports = {
    usuarios_list: (req, res, next) => {
        Usuario.find({}, (err, usuarios) => {
            res.render('usuarios/index', {usuarios: usuarios});
        });
    },
    usuarios_update_get: (req, res, next) => {
        Usuario.findById(req.params.id, (err, usuario) => {
            res.render('usuarios/update', {errors: {}, usuario: usuario});
        });
    },
    usuarios_update_post: (req, res, next) => {
        var update_values = {nombre: req.body.nombre};
        Usuario.findByIdAndUpdate(req.params.id, update_values, (err, usuario) => {
            if(err) {
                console.log(err);
                res.render('usuarios/update', {errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
            }
            else {
                res.redirect('/usuarios');
                return;
            }
        });
    },
    usuarios_create_get: (req, res, next) => {
        res.render('usuarios/create', {errors: {}, usuario: new Usuario()});
    },
    usuarios_create_post: (req, res, next) => {
        if(req.body.password != req.body.confirm_password) {
            res.render('usuarios/create', {errors: {confirm_password: {message: 'Las contraseñas no coinciden'}}, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
            return;
        }
        Usuario.create({ nombre: req.body.nombre, email: req.body.email, password: req.body.password}, (err, nuevoUsuario) => {
            if(err) {
                console.log(err);
                res.render('usuarios/create', {errors: err, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
            }
            else {
                nuevoUsuario.enviar_email_bienvenida();
                res.redirect('/usuarios');
            }
        });
    },
    usuarios_delete: (req, res, next) => {
        Usuario.findByIdAndDelete(req.body.id, function(err) {
            if(err)
                next(err);
            else
                res.redirect('/usuarios');
        });
    }
}

// exports.usuarios_list = function(req, res,){
//     console.log('obteniendo usuarios');
//     Usuario.find({}, (err, users) => {
//         res.status(200).json({
//             usuarios : users
//         });
//     });
// };

// exports.usuarios_create = function(req, res){
//     console.log('creando user');
//     var user = new Usuario({nombre: req.body.nombre});

//     user.save((err) => {
//         res.status(200).json({user});
//     });
// };

// exports.usuario_reservar = (req, res) => {
//     Usuario.findById(req.body.id , (err, user) => {
//         console.log(user);
//         user.reservar(req.body.id ,req.body.idBici, req.body.fecha_ini, req.body.fecha_fin, (err) => {
//             console.log('reservando');
//             res.status(200).send();
//         });
//     });
// };