var mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
var Reserva = require('./reserva');
var Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
const crypto = require('crypto');

const Token = require('../models/token');

const mailer = require('../mailer/mailer');

const saltRounds = 10;

const validateMail = (email) => {
    const regexp = /^\w+([\.-]?w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return regexp.test(email);
}

var usuarioShema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validateMail , 'Por favor ingrese un email valido'],
        match: [/^\w+([\.-]?w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
    },
    password: {
        type: String,
        required: [true, 'La contraseña es obligatoria']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    },
    googleId: String,
    facebookId: String
});

usuarioShema.plugin(uniqueValidator, {message: 'El {PATH} ya existe con otro usuario'});

usuarioShema.pre('save', function (next) {
    if(this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioShema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
}

usuarioShema.methods.reservar = function (idBici, fecha_inicial, fecha_final, cb) {
    var reserva = new Reserva({usuario: this.id, bicicleta: idBici, fecha_inicial: fecha_inicial, fecha_final: fecha_final});
    console.log(reserva);
    reserva.save(cb);
}

usuarioShema.methods.enviar_email_bienvenida = function (cb) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save((err) => {
        if(err) {return console.log(err.message);}

        const mailOptions = {
            from:  'santiago.hernandez124@gmail.com',
            to: email_destination,
            subject: 'Verificacion de cuenta',
            text: 'Hola, \n\n'+ 'Porfavor, para verificar su cuenta ingrese aqui: ' + process.env.HOST + '\/token/confirmation\/' + token.token + '.\n'
        };

        mailer.sendMail(mailOptions, (err) => {
            if(err) {return console.log(err.message);}

            console.log('A verification email has been sent to ' + email_destination);
        });
    });
}

usuarioShema.methods.resetPassword = function (cb) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save((err) => {
        if(err) {return console.log(err.message);}

        const mailOptions = {
            from:  'reset-no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Recuperacion de contraseña',
            text: 'Hola, \n\n'+ 'Porfavor, para recueperar su contraseña ingrese aqui: \n ' + process.env.HOST + '\/resetPassword\/' + token.token + '.\n'
        };

        mailer.sendMail(mailOptions, (err) => {
            if(err) {return cb(err);}

            console.log('Se envió un email para la recuperacion de la contraseña ' + email_destination);
        });
        cb(null);
    });
}

usuarioShema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback) {
    console.log('////////// CONDITION ///////////');
    console.log(condition);
    const self = this;
    console.log(condition);
    self.findOne({
        $or: [
            {'googleId': condition.id},
            {'email': condition.emails[0].value}
        ]
    },
    (err, result) => {
        if(result) {
            callback(err, result)
        }
        else {
            console.log('////////// CONDITION ///////////');
            console.log(condition);
            let values = {};
            values.googleId = condition.id;
            values.email = condition.emails[0].value;
            values.nombre = condition.displayName || 'SIN NOMBRE';
            values.verificado = true;
            values.password = condition._json.etag;
            console.log('///////// VALUES //////////');
            console.log(values);
            self.create(values, (err, result) => {
                if(err) {console.log(err);}
                return callback(err, result)
            })
        }
    })    
}

usuarioShema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback) {
    console.log('////////// CONDITION ///////////');
    console.log(condition);
    const self = this;
    self.findOne({
        $or: [
            {'facebookId': condition.id},
            {'email': condition.emails[0].value}
    ]},
    (err, result) => {
        if(result) {
            callback(err, result)
        }
        else {
            console.log('////////// CONDITION ///////////');
            console.log(condition);
            let values = {};
            values.facebookId = condition.id;
            values.email = condition.emails[0].value;
            values.nombre = condition.displayName || 'SIN NOMBRE';
            values.verificado = true;
            values.password = crypto.randomBytes(16).toString('hex');
            console.log('///////// VALUES //////////');
            console.log(values);
            self.create(values, (err, result) => {
                if(err) {console.log(err);}
                return callback(err, result)
            })
        }
    })    
}

module.exports = mongoose.model('Usuario', usuarioShema);