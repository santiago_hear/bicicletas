var mongoose = require('mongoose');
var moment = require('moment');
var Schema = mongoose.Schema;

var reservaSchema = new Schema({
    fecha_inicial: Date,
    fecha_final: Date,
    bicicleta: {type: mongoose.Schema.Types.ObjectId, ref: 'Bicicleta'},
    usuario: {type: mongoose.Schema.Types.ObjectId, ref: 'Usuario'}
});

reservaSchema.methods.diasDeReserva = () => {
    return moment(this.fecha_final).diff(moment(this.fecha_inicial), 'days') + 2;
}

module.exports = mongoose.model('Reserva', reservaSchema);