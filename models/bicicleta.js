//////////////// ESQUEMA MONGO DB CON MONGOOSE //////////////////

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BicicletaSchema = new Schema({
    idBici: Number,
    color: String,
    modelo: String,
    ubicacion : {
        type: [Number], index: { type : '2dsphere', sparse: true} 
    }
});

BicicletaSchema.statics.createInstance = function (idBici, color, modelo, ubicacion) {
    return new this({
        idBici : idBici,
        color : color,
        modelo : modelo,
        ubicacion : ubicacion
    });
};

BicicletaSchema.methods.toString = function () {
    return 'id: ' + this.idBici + " | color: " + this.color;
};

BicicletaSchema.statics.allBicis = function (cb) {
    console.log('obteniendo bicis');
    return this.find({}, cb);
};

BicicletaSchema.statics.add = function (Bici, cb) {
    this.create(Bici, cb);
};

BicicletaSchema.statics.findById = function (IdBici, cb) {
    return this.findOne({idBici : IdBici}, cb);
};

BicicletaSchema.statics.removeById = function (IdBici, cb) {
    return this.deleteOne({idBici : IdBici}, cb);
};

module.exports = mongoose.model('Bicicleta', BicicletaSchema);

////////////////////// ESQUEMA COMUN //////////////////////////

// var Bicicleta = function(id, color, modelo, ubicacion) {
//     this.id = id;
//     this.color = color;
//     this.modelo = modelo;
//     this.ubicacion = ubicacion;
// }

// Bicicleta.prototype.toString = function () {
//     return 'id: ' + this.id + " | color: " + this.color;
// }

// Bicicleta.allBicis = [];

// Bicicleta.add = function(Bici){
//     Bicicleta.allBicis.push(Bici);
// }

// Bicicleta.findById = function(BiciId){
//     var Bici = Bicicleta.allBicis.find(b => b.id == BiciId);
//     if(Bici)
//         return Bici;
//     else
//         throw new Error('No existe una la bicicleta con id '+ BiciId);
// }

// Bicicleta.removeById = function(BiciId){
//     for (var i = 0; i < Bicicleta.allBicis.length; i++){
//         if (Bicicleta.allBicis[i].id == BiciId)
//             Bicicleta.allBicis.splice(i,1);
//             break;
//     } 
// }

// // var a = new Bicicleta(1, 'rojo', 'urbana', [5.067455, -75.524063]);
// // var b = new Bicicleta(2, 'blanco', 'urbana', [5.068043, -75.511789]);

// // Bicicleta.add(a);
// // Bicicleta.add(b);

// module.exports = Bicicleta;