const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');

// const mailConfig = {
//     host: 'smtp.ethereal.email',
//     port: 587,
//     auth: {
//         user: 'roman63@ethereal.email',
//         pass: 'P344B8kMKyXZsmdGnx'
//     }
// };

let mailConfig;

if(process.env.NODE_ENV === 'production') {
    const options = {
        auth: {
            api_key: process.env.SENDGRID_API_SECRET
        }
    }
    mailConfig = sgTransport(options);
}
else {
    if(process.env.NODE_ENV === 'staging') {
        console.log('XXXXXXXXXXXXXX');
        const options = {
            auth: {
                api_key: process.env.SENDGRID_API_SECRET
            }
        }
        mailConfig = sgTransport(options);
    }
    else {
        // all mails are catched by ethereal.email
        mailConfig = {
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: process.env.ethereal_user, //'roman63@ethereal.email',
                pass: process.env.ethereal_pswd //'P344B8kMKyXZsmdGnx'
            }
        };
    }
}

module.exports = nodemailer.createTransport(mailConfig);